CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE article (
    ean varchar PRIMARY KEY not null ,
    nom varchar not null,
    marque varchar not null,
    url varchar not null,
    dimensions varchar not null,
    description varchar null,
    categorie varchar not null,
    classe_energetique char,
    conso_annuelle float not null,
    image varchar not null
);

CREATE TABLE utilisateur (
    uuid_ uuid DEFAULT uuid_generate_v4(),
    mail varchar UNIQUE not null,
    mdp varchar not null,
    PRIMARY KEY (uuid_)
);

CREATE TABLE userList (
    article_ean varchar not null,
    user_uuid uuid not null,
    quantite int not null,
    FOREIGN KEY (article_ean) REFERENCES article(ean),
    FOREIGN KEY (user_uuid) REFERENCES utilisateur(uuid_),
    PRIMARY KEY (user_uuid, article_ean)
);

INSERT INTO utilisateur (mail, mdp) VALUES ('test@test.com', '1234');
INSERT INTO article (ean, nom, marque, url, dimensions, description, categorie, classe_energetique, conso_annuelle, image)
VALUES ('B08J44R34X', 'Bosch KGV58VLEAS Série 4, Réfrigérateur combiné pose-libre - 503 L - H 191 X L 70 cm - Inox','Bosch Electroménager',
        'https://www.amazon.fr/BOSCH-Refrigerateurs-combines-inverses-KGV58VLEAS/dp/B08J44R34X', '770 x 70 x 191 cm; 88 kilogrammes',
        'Tension: 240 Volts. Dimensions du produit: 70P x 77l x 191H centimètres. Système de dégivrage: Automatique. Nombre de portes: 2. Type d''installation: Autonome. Couleur: Couleur Inox. Energy Star: 4 Étoile.',
        'Frigo', 'F', 100, 'https://m.media-amazon.com/images/I/616AgTvq-TL.jpg')
