const { response } = require('express')

const Pool = require('pg').Pool
const pool = new Pool({
  user: 'fuse',
  host: 'localhost',
  database: 'fuse',
  password: 'fuse',
  port: 5438,
})

const getUsers = (req, res) => {
    pool.query('SELECT * FROM utilisateur', (error, results) => {
        if (error) {
            throw error
        }
        res.status(200).json(results.rows)
    })
}

const getUserByUuid = (req, res) => {
    pool.query('SELECT * FROM utilisateur WHERE uuid_ = $1', [req.params.uuid], (error, results) => {
        if (error) {
            throw error
        }
        res.status(200).json(results.rows)
    })
}

const getUserByMailAndPassword = (req, res) => {
    const mail = req.body.mail;
    const mdp = req.body.mdp;
    console.log(req.body);
    pool.query('SELECT * FROM utilisateur WHERE mail = $1 AND mdp = $2', [mail, mdp], (error, results) => {
        if (error) {
            throw error
        }
        res.status(200).json(results.rows)
    })
}

const getUserArticleList = (req, res) => {
    pool.query('SELECT * FROM userList where user_uuid = $1', [req.param.uuid], (error, results) => {
        if (error) {
            throw error
        }
        res.status(200).json(results.rows)
    })
}

module.exports = {
    getUsers,
    getUserByUuid,
    getUserByMailAndPassword,
    getUserArticleList,
}