var express = require('express');
const Article = require('../Model/Article');
var article = new Article;
var router = express.Router();

/* GET article by EAN */
router.get('/:ean', article.articleExist);

/* GET articles by cartegories */
// TODO add some queries like EEC limitation and maybe max price
router.get('/', function(req, res){
    res.send('This will get you a list of article sorted by their Energy Efficiency Class')
})

module.exports = router;