const axios = require('axios');
const Article = require("../Model/Article");

const params = {
    api_key: "9EE946FFABCC400C8DD44468970BE384",
    amazon_domain: "amazon.fr",
    asin: "B092HJXNR3",
    type: "product"
}

let article

axios.get('https://api.rainforestapi.com/request', { params })
    .then(response => {

        let result = response.data["product"]

        let nom = result["title"]
        let marque = result["brand"]
        let url = result["link"]
        let dimensions = result["dimensions"]
        let categorie = result["categories"][1]["name"]
        let desc = result["specifications_flat"]
        let conso
        let classeEng
        let spec = result["specifications"]
        let imgUrl = result["main_image"]["link"]
        for(let i = 0; i < spec.length; i++){
            if(spec[i]["name"] === "Consommation électrique annuelle"){
                conso = spec[i]["value"]
            }
            if(spec[i]["name"] === "Efficacité énergétique (7 niveaux)"){
                classeEng = spec[i]["value"]
            }
        }
        if (conso.length !== 0 || classeEng !== ""){
            article = new Article(nom, marque, url, dimensions, categorie, desc, conso, params.asin, classeEng, imgUrl)
            console.log(article)
        }
    })
    .catch(error => {
    console.log(error);
    })

module.exports = article
