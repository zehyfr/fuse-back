var express = require('express');
var db = require('../db/queries')
var router = express.Router();

/* GET user info */
router.get('/:uuid', db.getUserByUuid);

/* GET user list of article */
router.get('/:uuid/articles/', db.getUserArticleList);

/* PUT update list of an user */
router.post('/:uuid/artciles/', function(req,res) {
  res.send('This will update the list of articles of an user')
});

router.post('/', db.getUserByMailAndPassword)

router.get('/', db.getUsers);

module.exports = router;
