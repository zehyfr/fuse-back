const Pool = require('pg').Pool

const pool = new Pool({
    user: "fuse",
    password: "fuse",
    database: "fuse",
    host: "localhost",
    port: 5438,
})

module.exports = {pool}
