const {pool} = require("../config/db.config");

class Article{

    // Constructeur
    constructor(nom, marque, url, dimensions, categorie, desc, conso, ean, classeEng, imgUrl) {
        this.ean = ean
        this.nom = nom
        this.marque = marque
        this.url = url
        this.dimensions = dimensions
        this.categorie = categorie
        this.desc = desc
        this.conso = conso
        this.classeEng = classeEng
        this.imgUrl = imgUrl
    }

    /**
     * Insère un article
     * @param article
     * @returns {Promise<void>}
     */
    insertArticle(article){

        pool.query(
            "INSERT INTO article (ean, nom, marque, url, dimensions, description, categorie, classe_energetique, conso_annuelle, image) " +
            "VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)",
            [article.ean, article.nom, article.marque, article.url, article.dimensions, article.desc, article.categorie, article.classeEng, article.conso, article.imgUrl],
            (error, results) => {
                if(error){
                    throw error
                } else {
                    console.log("Insert ok pour article : " + article.nom)
                }

            }
        )
    }

    /**
     * Vérifie si l'article est déjà présent en bdd
     */
    articleExist = (req, res) => {
        pool.query("SELECT * FROM article WHERE ean = $1", [req.params.ean], (error, results) => {
            if(error){
                throw error
            }
            console.log(results.rows)
            res.status(200).json(results.rows)
        })
    }

    /**
     * Récupère une liste d'articles en fonction d'une catégorie
     * @param categorie
     * @return articles
     */
    getArticlesParCategorie(categorie){
        pool.query("SELECT * FROM article WHERE categorie = $1",
            [categorie],
            (error, results) => {
                if(error){
                    throw error
                }
                console.log(results.rows)
                return results.rows
            }
        )
    }
}

module.exports = Article
